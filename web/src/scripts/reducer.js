define("reducer", ["app/actions", "app/dao", "seamless-immutable", "diff"], function(Actions, Dao, Immutable, Diff) {
    return function(state, action) {
        console.log(action);
        switch (action.type) {
            case Actions.CHANGE_ROUTE:
              var newState = state.asMutable({deep: true});
              newState.route = action.data.route;
              newState.documentFocus = {id: undefined, editing: true};
              if(action.data.route.indexOf("/documents") !== -1 && action.data.route.split("/").length > 2) {
                newState.documentFocus = {id: action.data.route.split("/")[2], editing: false};
              }
              return Immutable(newState);
            case Actions.EDIT_DOC:
              return state.setIn(["documentFocus","editing"], true);
            case Actions.VIEW_DOC:
              return state.setIn(["documentFocus","editing"], false);
            case Actions.SAVE_DOC:
              var oldDoc = Dao.getDocument(action.data.id);
              if(action.data.id !== undefined && oldDoc.document.content !== action.data.content) {
                var deltas = Diff.diffWords(oldDoc.document.content, action.data.content);
                //var deltas = [];

                for (var i = 0; i < oldDoc.relationships.parents.length; i++) {
                  Dao.addAlert(oldDoc.relationships.parents[i].parent, action.data.id, "How", deltas);
                }
                for (var i = 0; i < oldDoc.relationships.children.length; i++) {
                  Dao.addAlert(oldDoc.relationships.children[i].child, action.data.id, "Why", deltas);
                }
              } 
              var newDoc = Dao.saveDocument(action.data);
              return state.merge({route: "#/documents/" + newDoc.id, 
                documentFocus: {id: newDoc.id, editing: false}, documents: Dao.getAllDocuments()});
            case Actions.DELETE_DOC:
              Dao.deleteDocument(action.data);
              return state.merge({documents: Dao.getAllDocuments(), relationships: Dao.getAllRelationships(), 
                route: "#/", documentFocus: {id: undefined, editing: false}});
            case Actions.ADD_RELATION:
              Dao.saveRelation(action.data);
              return state.set("relationships", Dao.getAllRelationships());
            case Actions.DELETE_RELATION:
              Dao.deleteRelation(action.data);
              return state.set("relationships", Dao.getAllRelationships());
            case Actions.DISMISS_ALERT:
              Dao.deleteAlert(action.data.alertedDoc, action.data.changedDoc);
              return state;
            default:
              return state;
        }
    };
});
