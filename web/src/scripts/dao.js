define("dao", ["./config.js", "node-uuid"], function(Config, Uuid) {
    getItems = function(itemName) {
        return JSON.parse(localStorage.getItem(Config.namespace + "." + itemName) || "[]");
    };

    saveItems = function(itemName, items) {
        localStorage.setItem(Config.namespace + "." + itemName, JSON.stringify(items));
    };

    var documents = getItems("documents");
    if (Object.keys(documents).length === 0) {
        documents = {
            "39802u8apu":{"title":"Mission Statement","content":"<div>To make the durable and innovative widgets that delight our customers.</div>","id":"39802u8apu"},
            "fda908djafp":{"title":"Durability","content":"<div>Our widgets will have a less than 1% fail rate in the first five years of use or our customers get a refund.  Widgets should be robust.</div>","id":"fda908djafp"},
            "eowpfdsjl97":{"title":"Innovation","content":"<div>We strive for innovative products that exceed customer expectations and competitor's features.</div>","id":"eowpfdsjl97"},
            "9a8ufpupo8":{"title":"Quality Assurance","content":"<div>A team will be dedicated to ensuring the durability of our widgets through rigorous testing.</div>","id":"9a8ufpupo8"},
            "4359fadsjls":{"title":"Speak up for quality program","content":"<div>All employees are encouraged to alert their managers of any potential defects discovered in our widgets regardless of budgets or schedules.</div>","id":"4359fadsjls"},
            "0f6bd2e0-173c-11e6-89d5-557551a431ee":{"title":"Skunkworks","content":"<div>A team of highly talented engineers will be tasked with coming up with innovative product and process improvements that border impossibility.</div>","id":"0f6bd2e0-173c-11e6-89d5-557551a431ee"}};
    }

    var relationships = getItems("relationships");
    if (relationships.length === 0) {
        relationships = [
        {"id":"39802u8apu","parent":"39802u8apu","child":"fda908djafp","parentRoleName":"why","childRoleName":"how","relatedSlug":"Mission Statement"},
        {"id":"fda908djafp","parent":"39802u8apu","child":"eowpfdsjl97","parentRoleName":"why","childRoleName":"how"},
        {"id":"eowpfdsjl97","parent":"fda908djafp","child":"9a8ufpupo8","parentRoleName":"why","childRoleName":"how"},
        {"id":"pa907alhjh","parent":"fda908djafp","child":"4359fadsjls","parentRoleName":"why","childRoleName":"how"}];
    }

    var alerts = getItems("alerts");
    if (Object.keys(alerts).length === 0) {
        alerts = {};
    }

    return {
        source: {
            documents: documents,
            relationships: relationships,
            alerts: alerts
        },
        getAllDocuments: function() {
            return this.source.documents;
        },
        getAllRelationships: function() {
            return this.source.relationships;
        },
        getDocument: function(docID, lazy) {
            var selectedDoc;
            if (this.source.documents[docID] === undefined) {
                return undefined
            }

            if (lazy) {
                return this.source.documents[docID];
            }

            selectedDoc = { document: this.source.documents[docID], 
                relationships: {parents: [], children: [], available: []},
                alerts: [] };

            for (var i = 0; i < this.source.relationships.length; i++) {
                if (this.source.relationships[i].parent === selectedDoc.document.id) {
                    this.source.relationships[i].relatedSlug = this.source.documents[this.source.relationships[i].child].title;
                    selectedDoc.relationships.children.push(this.source.relationships[i]);
                } else if (this.source.relationships[i].child === selectedDoc.document.id) {
                    this.source.relationships[i].relatedSlug = this.source.documents[this.source.relationships[i].parent].title;
                    selectedDoc.relationships.parents.push(this.source.relationships[i]);
                }
            }

            var docKeys = Object.keys(this.source.documents);
            for (var i = 0; i < docKeys.length; i++) {
                if (docKeys[i] === selectedDoc.document.id) {
                    continue;
                }
                var found = false;
                for (var j = 0; j < selectedDoc.relationships.parents.length; j++) {
                    if (docKeys[i] === selectedDoc.relationships.parents[j].parent) {
                        selectedDoc.relationships.parents[j].parent;
                        found = true;
                        break;
                    }
                }
                if (found) {
                    continue;
                }

                for (var j = 0; j < selectedDoc.relationships.children.length; j++) {
                    if (docKeys[i] === selectedDoc.relationships.children[j].child) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    continue;
                }
                selectedDoc.relationships.available.push(this.source.documents[docKeys[i]]);
            }
            if (this.source.alerts[docID]) {
            var alertKeys = Object.keys(this.source.alerts[docID]);
            for (var i = 0; i < alertKeys.length; i++) {
                selectedDoc.alerts.push(this.source.alerts[docID][alertKeys[i]]);
            }
        }

            return selectedDoc;
        },
        saveDocument: function(document, relationships) {
            if (document["id"] === undefined) {
                document.id = Uuid.v1();
            }
            this.source.documents[document.id] = document;
            
            if (relationships !== undefined) {
                for (var i = 0; i < relationships; i++) {
                    this.saveRelation(relationships[i]);
                }
            }
            this.save();
            return document;
        },
        deleteDocument: function(id) {
            delete this.source.documents[id];
            delete this.source.alerts[id];
            for(var i = this.source.relationships.length - 1; i >= 0; i--) {
                if(this.source.relationships[i].parent === id || this.source.relationships[i].child === id) {
                    this.source.relationships.splice(i, 1);
                }
            }
            this.save();
        },
        saveRelation: function(relationship) {
            if (relationship.id === undefined) {
                relationship.id = Uuid.v1();
            }

            this.source.relationships.push(relationship);
            this.save();
            return relationship;
        },
        deleteRelation: function(relationID) {
            for (var i = 0; i < this.source.relationships.length; i++) {
                if (this.source.relationships[i].id === relationID) {
                    this.source.relationships.splice(i, 1);
                    this.save();
                    return;
                }
            }
        },
        addAlert: function(alertedID, sourceID, roleName, deltas) {
            if (this.source.alerts[alertedID] == undefined) {
                this.source.alerts[alertedID] = {};
            }

            this.source.alerts[alertedID][sourceID] = {
                id: sourceID,
                relatedSlug: this.getDocument(sourceID, true).title,
                relatedRoleName: roleName,
                deltas: deltas
            };
            this.save();
        },
        getAlertCount: function(docID) {
            return this.source.alerts[docID] ? Object.keys(this.source.alerts[docID]).length : 0;
        },
        deleteAlert: function(alertedID, sourceID) {
            delete this.source.alerts[alertedID][sourceID];
            this.save();
        },
        save: function() {
            saveItems("documents", this.source.documents);
            saveItems("relationships", this.source.relationships);
            saveItems("alerts", this.source.alerts);
        }
    }
});
