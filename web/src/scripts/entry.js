define("main", ["app/reducer", "app/stateManager", "app/viewManager", "app/actions", "redux"],
    function(Reducer, State, Views, Actions, Redux) {
        var reducer = Redux.createStore(Reducer, State.get());
        Views.setReducer(reducer);

        // not sure how this fits into the redux flow but it works for now
        window.onhashchange = function(e) {
            var newHash = e.newURL.substr(e.newURL.indexOf("#"));

            if (reducer.getState().route !== newHash) {
                reducer.dispatch(Actions.build(Actions.CHANGE_ROUTE, {route: newHash}));
            }
        };

        console.log("app started");
    });
require(["main"], function(main) {});
