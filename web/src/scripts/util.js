define("util", ["app/actions"], function(Actions) {
    return {
        removeClass: function(selector, className) {
            var links = document.querySelectorAll(selector),
                regex = new RegExp("(?:^|\\s)" + className + "(?!\\S)", "g");
            for (var i = 0; i < links.length; i++) {
                links[i].className = links[i].className.replace(regex, '');
            }
        },
        applyTemplate: function(container, renderedTemplate) {
            container.innerHTML = renderedTemplate;
            window.componentHandler.upgradeDom();
        }
    };
});
