define("viewManager", ["app/views/routeManager", "app/views/master"], 
    function(RouteManager, Master) {
    return {
        views: {
            route: { controller: RouteManager, unsubscribe: null },
            master: { controller: Master, unsubscribe: null },
        },
        setReducer: function(reducer) {
            var manager = this;
            var viewKeys = Object.keys(manager.views);

            document.onreadystatechange = function() {
                if(document.readyState === "complete") {
                    for (var i = 0; i < viewKeys.length; i++) {
                    var currentView = manager.views[viewKeys[i]];
                    currentView.controller = currentView.controller(reducer);
                    currentView.unsubscribe = reducer.subscribe(currentView.controller);
                    currentView.controller();
                }
                }
            };
        }
    }
});
