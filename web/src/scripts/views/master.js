define("master", ["app/util", "app/views/documentMap", "app/views/documentEdit"],
    function(Util, DocumentMap, DocumentEdit) {
        return function(store) {
          var container = document.querySelector("#page-content"),
            mapView = DocumentMap(store, container),
            docView = DocumentEdit(store, container);

          return function() {
            var route = store.getState().route;

            Util.removeClass("a.active", "active");

            if(route === "#/") {
              document.querySelector('a[href^="#/"]').className += " active";
              mapView();
            } else if(route.indexOf("#/documents") !== -1) {
              document.querySelector('a[href^="#/documents"]').className += " active";
              docView();
            }
          };
        };
    });
