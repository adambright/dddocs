define("routeManager", [], function() {
    return function(store) {
      return function() {
        if(store.getState().route !== window.location.hash) {
            history.pushState({}, "", store.getState().route);
        }
      };
    };
});