define("documentMapView", ["app/config", "app/util", "app/actions", "app/dao", "templates/smallDoc.mustache"],
    function(Config, Util, Actions, Dao, itemTemplate) {
        return function(store, container) {
            function computeLayer(docs, rels, currentLayer) {
                for(var i = 0; i < rels.length; i++) {
                    if(docs[rels[i].source].layer == currentLayer && docs[rels[i].target].layer == 0) {
                        docs[rels[i].target].layer = currentLayer + 1;
                    }
                }
            }

            function checkLayers(docs) {
                for(var i = 0; i < docs.length; i++) {
                    if(docs[i].layer == 0) {
                        return false;
                    }
                }
                return true;
            }

            var maxLayer = 1;

            return function() {
                var mapEl,
                    w,
                    h,
                    docs = store.getState().documents,
                    rels = store.getState().relationships,
                    layerMap = {},
                    nodes = [],
                    links = [];

                require(["templates/documentMap.mustache"], function(Template) {
                    Util.applyTemplate(container, Template({}));

                    document.querySelector("#add-document-fab").addEventListener("click", function(e) {
                        store.dispatch(Actions.build(Actions.CHANGE_ROUTE, {route: "#/documents"}));
                    });

                    mapEl = document.querySelector("#document-map");
                    w = mapEl.offsetWidth;
                    h = mapEl.offsetHeight;

                    // prevent root nodes from going off the screen
                    nodes.push({id: 0, fixed: true, x: -120, y: 50});
                    nodes.push({id: 0,  fixed: true, x: w + 10, y: 50});

                    var docKeys = Object.keys(docs);

                    for (var i = 0; i < docKeys.length; i++) {
                        var node = docs[docKeys[i]].asMutable();

                        node.alerts = Dao.getAlertCount(node.id);
                        node.noAlerts = node.alerts === 0;
                        node.root = true;
                        node.orphan = true;
                        node.x = w / 2 + i * 10;
                        node.y = 50;
                        nodes.push(node);
                    }

                    var rootNodes = nodes.length;

                    for (var i = 0; i < rels.length; i++) {
                        var link = { index: i };

                        for (var j = 0; j < nodes.length; j++) {
                            if (rels[i].parent === nodes[j].id) {
                                link.source = j;
                                nodes[j].orphan = false;
                                if (!nodes[j].root) {
                                    nodes[j].y += 50;
                                }
                            } else if (rels[i].child === nodes[j].id) {
                                link.target = j;
                                nodes[j].orphan = false;
                                nodes[j].root = false;
                                rootNodes--;
                                layerMap[nodes[j].id] = undefined;
                            }

                            if (link.source !== undefined && link.target !== undefined) {
                                break;
                            }
                        }

                        links.push(link);
                    }
                    /*
                    var rootNodesLeft = rootNodes;
                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].root) {
                            nodes[i].x = w / (rootNodes + 1) * (rootNodes - rootNodesLeft + 1);
                            rootNodesLeft--;
                        }
                    }

                    
                    var i = 0;
                    while(!checkLayers(nodes) && i < 10) {
                        computeLayer(nodes, links, maxLayer);
                        maxLayer = maxLayer + 1;
                        i = i + 1;
                    }
                    */

                    require(["d3"], renderMap);
                });

                function renderMap(d3) {
                    var container = d3.select("#document-map").append("svg").attr({ width: w, height: h })
                    elWidth = 100;
                    elHeight = 40;

                    var map = d3.layout.force()
                        .size([w, h])
                        .nodes(nodes)
                        .charge(function(node) {
                            return node.orphan !== undefined && node.orphan ? -200 : -5000
                        })
                        .chargeDistance(300)
                        .links(links)
                        .linkDistance(100)
                        .linkStrength(8)
                        .friction(.6);

                    var link = container.selectAll('.link')
                        .data(links)
                        .enter().append("line")
                        .attr({ class: "link" });

                    var node = container.selectAll("g")
                        .data(nodes)
                        .enter()
                        .append("g")
                        .attr("class", "node");

                    node.append("foreignObject")
                        .attr({
                            width: elWidth,
                            height: elHeight
                        })
                        .append("xhtml:body")
                        .html(function(d) {
                            return itemTemplate({ link: "#/documents/" + d.id, slug: d.title, alerts: d.alerts, noAlerts: d.noAlerts });
                        });;

                    map.on('tick', function(e) {      
                        var k = .05;
                        for(var i = 0; i < nodes.length; i++) {
                            if(!nodes[i].root && !nodes[i].orphan) {
                                nodes[i].y = nodes[i].y + k;
                            }
                            if(nodes[i].orphan) {
                                if(nodes[i].x <= w - elWidth) {
                                    nodes[i].x = nodes[i].x + 1;
                                } else {
                                    nodes[i].x = w - elWidth;
                                }
                            }
                        }

                        container.selectAll('.node > foreignObject')
                            .attr({
                                x: function(d) {
                                    return d.x;
                                },
                                y: function(d) {
                                    return d.root ? 50 : d.y;
                                }
                            });


                        link.attr({
                            x1: function(d) {
                                return d.source.x  + elWidth / 2;
                            },
                            y1: function(d) {
                                return (d.source.root ? 50 : d.source.y) + elHeight / 2;
                            },
                            x2: function(d) {
                                return d.target.x  + elWidth / 2;
                            },
                            y2: function(d) {
                                return d.target.y + elHeight / 2;
                            }});
                    });
                    
                    document.querySelector("img.loading").style.display = "none";
                    map.start();
                    window.componentHandler.upgradeDom();
                };
            };
        };
    });
