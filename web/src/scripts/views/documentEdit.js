define("documentEdit", ["app/util", "app/dao", "app/actions", "quill"],
    function(Util, Dao, Actions, Quill) {
        return function(store, container) {
            function startEditMode(container, doc) {
                require(["templates/documentEdit.mustache"], function(Template) {
                    Util.applyTemplate(container, Template(doc));

                    var editor = new Quill("#doc-content");
                    editor.addModule("toolbar", { container: "#toolbar" });
                    if(doc.document !== undefined && doc.document.content !== undefined) {
                        editor.setHTML(doc.document.content);
                    }

                    function saveDocument() {
                        var payload = {
                                title: document.querySelector("#doc-title").value,
                                content: editor.getHTML()
                            };
                        if (doc !== undefined 
                            && doc.document !== undefined
                            && doc.document.id !== undefined) {
                            payload.id = doc.document.id;
                        }
                        store.dispatch(Actions.build(Actions.SAVE_DOC, payload));
                        return payload;
                    }

                    document.querySelector("#save-button").addEventListener("click", function() { saveDocument(); });
                    document.querySelector("#delete-button").addEventListener("click", function(e) {
                        store.dispatch(Actions.build(Actions.DELETE_DOC, doc !== undefined && doc.document !== undefined 
                            ? doc.document.id : undefined));
                    });
                    document.querySelector("#done-button").addEventListener("click", function() {
                        var newDoc = saveDocument();
                        store.dispatch(Actions.build(Actions.VIEW_DOC));
                    });
                });
            }

            function startViewMode(container, doc) {
                require(["templates/documentView.mustache"], function(Template) {
                    Util.applyTemplate(container, Template(doc));
                    
                    document.querySelector("#edit-button").addEventListener("click", function() {
                        store.dispatch(Actions.build(Actions.EDIT_DOC));
                    });

                    var related = document.querySelectorAll(".related-button");
                    for (var i = 0; i < related.length; i++) {
                        related[i].addEventListener("click", function(e) {
                            store.dispatch(Actions.build(Actions.CHANGE_ROUTE, {route: e.target.getAttribute("data-link")}));
                        });
                    }

                    var newParent = document.querySelectorAll(".add-parent");
                    for (var i = 0; i < newParent.length; i++) {
                        newParent[i].addEventListener("click", function(e) {
                            store.dispatch(Actions.build(Actions.ADD_RELATION, 
                                {
                                    "parent": e.target.getAttribute("data-link"),
                                    "child": doc.document.id,
                                    "parentRoleName":"why","childRoleName":"how"
                                }));
                        });
                    }

                    var newChild = document.querySelectorAll(".add-child");
                    for (var i = 0; i < newChild.length; i++) {
                        newChild[i].addEventListener("click", function(e) {
                            store.dispatch(Actions.build(Actions.ADD_RELATION, 
                                {
                                    "parent": doc.document.id,
                                    "child": e.target.getAttribute("data-link"),
                                    "parentRoleName":"why","childRoleName":"how"
                                }));
                        });
                    }

                    var deleteRelation = document.querySelectorAll(".delete-relation-button");
                    for (var i = 0; i < deleteRelation.length; i++) {
                        deleteRelation[i].addEventListener("click", function(e) {
                            store.dispatch(Actions.build(Actions.DELETE_RELATION, e.target.getAttribute("data-relation")));
                        });
                    }

                    var dismissAlert = document.querySelectorAll(".dismiss-button");
                    for (var i = 0; i < dismissAlert.length; i++) {
                        dismissAlert[i].addEventListener("click", function(e) {
                            store.dispatch(Actions.build(Actions.DISMISS_ALERT, 
                                {
                                    alertedDoc: doc.document.id,
                                    changedDoc: e.target.getAttribute("data-relation")
                                }));
                        });
                    }
                });
            }

            return function() {
                var selectedDoc = {};

                if (store.getState().documentFocus.id !== undefined) {
                    selectedDoc = Dao.getDocument(store.getState().documentFocus.id);
                    selectedDoc.relationships.noChildren = selectedDoc.relationships.children.length === 0;
                    selectedDoc.relationships.noParents = selectedDoc.relationships.parents.length === 0;
                    selectedDoc.noAlerts = selectedDoc.alerts.length === 0;
                }
                if (store.getState().documentFocus.editing) {
                    startEditMode(container, selectedDoc);
                } else {
                    startViewMode(container, selectedDoc);
                }
            };
        };
    });
