define("stateManager", ["app/config", "app/dao", "seamless-immutable"], function (Config, Data, Immutable) {
    var localStorageKey = Config.namespace + ".state";

    return {
        default: Immutable({
            route: window.location.hash,
            documents: Data.getAllDocuments(),
            relationships: Data.getAllRelationships(),
            documentFocus: {id: undefined, editing: false}
        }),
        get: function() {
            var savedState = JSON.parse(localStorage.getItem(localStorageKey) || "{}");
            return this.default.merge(savedState);
        },
        set: function(state) {
            localStorage.setItem(localStorageKey, JSON.stringify(state));
        }
    };
});