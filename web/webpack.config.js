var path = require( 'path' );
const BUILD_DIR = __dirname + "/static/js/app";

module.exports = {
    context: __dirname,
    entry: "./src/scripts/entry.js",
    output: {
        path: BUILD_DIR,
        publicPath: "static/js/app/",
        filename: "main.js",
        chunkFilename: "[id].js"
    },
    resolve: {
        alias: {
            app: path.resolve(__dirname, "src", "scripts"),
            templates: path.resolve(__dirname, "static", "templates")
        },
        extensions: ["", ".js", ".mustache"]
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" },
            { test: /\.mustache$/, loader: "mustache"}
        ]
    }
};