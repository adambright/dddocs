CC=go
SERVER_PROJECT=bitbucket.org/adambright/dddocs
export GOPATH=${PWD}/app/go
export GO15VENDOREXPERIMENT=1

debug: 
	killall -9 main go node || true
	$(CC) run app/go/src/$(SERVER_PROJECT)/main.go config.yml &
	cd web; rm -rf static/js/*; webpack --progress --colors --watch &

build: clean
	$(CC) install $(SERVER_PROJECT)

deps:
	cd app/src/$(SERVER_PROJECT); glide install

.PHONY: clean
clean:
	rm -rf bin/*