# empty-project

Empty project.

## Building and running on localhost

First install dependencies:

```sh
npm install
```

NOTE: there is an issue after running `npm install`
In spa/node_modules/quill-delta/dist/Delta.d.ts replace the 'fast-diff' import with `import diff = require('fast-diff');`

To run in hot module reloading mode:

```sh
npm start
```

To create a production build:

```sh
npm run build-prod
```

To create a development build:

```sh
npm run build-dev
```

## Running

Open the file `dist/index.html` in your browser

## Credits

Made with [createapp.dev](https://createapp.dev/)
