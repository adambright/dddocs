import { AppState } from "../stateManager";
import { Store } from "redux";

export default function(store: Store<AppState>) {
  return function() {
    if (store.getState().route !== window.location.hash) {
      history.pushState({}, "", store.getState().route);
    }
  };
}
