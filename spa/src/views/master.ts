import { Store } from "redux";
import { AppState } from "../stateManager";
import * as Util from "../util";

export default function(store: Store<AppState>) {
  var container = document.querySelector("#page-content");
  //mapView = DocumentMap(store, container),
  //docView = DocumentEdit(store, container);

  return function() {
    var route = store.getState().route;

    Util.removeClass("a.active", "active");

    const root = document.querySelector('a[href^="#/"]'),
      documents = document.querySelector('a[href^="#/documents"]');
    if (route === "#/" && root) {
      root.className += " active";
      //mapView();
    } else if (route.indexOf("#/documents") !== -1 && documents) {
      documents.className += " active";
      //docView();
    }
  };
}
