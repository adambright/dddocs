import * as Util from "../util";
import { Store } from "redux";
import { AppState } from "../stateManager";
import * as Dao from "../dao";
import * as d3 from "d3";
import { ActionType, build } from "../actions";

const mapTemplate = require("../templates/documentMap.mustache");

const computeLayer = (docs: any, rels: any, currentLayer: number) => {
  for (var i = 0; i < rels.length; i++) {
    if (
      docs[rels[i].source].layer == currentLayer &&
      docs[rels[i].target].layer == 0
    ) {
      docs[rels[i].target].layer = currentLayer + 1;
    }
  }
};

const checkLayers = (docs: any[]) => {
  for (var i = 0; i < docs.length; i++) {
    if (docs[i].layer == 0) {
      return false;
    }
  }
  return true;
};

export default (store: Store<AppState>, container: Element) => {
  return function() {
    const maxLayer = 1;

    const docs = store.getState().documents,
      rels = store.getState().relationships,
      layerMap: { [key: string]: any } = {},
      nodes: any[] = [],
      links: any[] = [];

    Util.applyTemplate(container, mapTemplate({}));

    (
      document.querySelector("#add-document-fab") || new HTMLElement()
    ).addEventListener("click", function(e) {
      store.dispatch(build(ActionType.CHANGE_ROUTE, { route: "#/documents" }));
    });

    const mapEl = document.querySelector("#document-map") || new HTMLElement(),
      w = mapEl.clientWidth,
      h = mapEl.clientHeight;

    // prevent root nodes from going off the screen
    nodes.push({ id: 0, fixed: true, x: -120, y: 50 });
    nodes.push({ id: 0, fixed: true, x: w + 10, y: 50 });

    var docKeys = Object.keys(docs);

    for (let i = 0; i < docKeys.length; i++) {
      const node = docs[docKeys[i]] as any;

      node.alerts = Dao.getAlertCount(node.id);
      node.noAlerts = node.alerts === 0;
      node.root = true;
      node.orphan = true;
      node.x = w / 2 + i * 10;
      node.y = 50;
      nodes.push(node);
    }

    let rootNodes = nodes.length;

    for (let i = 0; i < rels.length; i++) {
      let link = { index: i } as any;

      for (let j = 0; j < nodes.length; j++) {
        if (rels[i].parent === nodes[j].id) {
          link.source = j;
          nodes[j].orphan = false;
          if (!nodes[j].root) {
            nodes[j].y += 50;
          }
        } else if (rels[i].child === nodes[j].id) {
          link.target = j;
          nodes[j].orphan = false;
          nodes[j].root = false;
          rootNodes--;
          layerMap[nodes[j].id] = undefined;
        }

        if (link.source !== undefined && link.target !== undefined) {
          break;
        }
      }

      links.push(link);
    }
    /*
                    var rootNodesLeft = rootNodes;
                    for (var i = 0; i < nodes.length; i++) {
                        if (nodes[i].root) {
                            nodes[i].x = w / (rootNodes + 1) * (rootNodes - rootNodesLeft + 1);
                            rootNodesLeft--;
                        }
                    }

                    
                    var i = 0;
                    while(!checkLayers(nodes) && i < 10) {
                        computeLayer(nodes, links, maxLayer);
                        maxLayer = maxLayer + 1;
                        i = i + 1;
                    }
                    */
  };
};
