import * as Util from "../util";
import * as Dao from "../dao";
import { ActionType, build } from "../actions";
import { AppState } from "../stateManager";
import { Store } from "redux";
import { Quill } from "quill";

export class SingleNodeView {
  constructor(
    private store: Store<AppState>,
    private container: HTMLElement,
    private selectedDoc?: Dao.NodeFamily
  ) {}

  private startViewMode(container: HTMLElement, doc: Dao.NodeFamily) {
    const self = this;

    const Template = require("../templates/documentView.mustache");
    Util.applyTemplate(container, Template(doc));

    (
      document.querySelector("#edit-button") || new HTMLElement()
    ).addEventListener("click", function() {
      self.store.dispatch(build(ActionType.EDIT_DOC));
    });

    var related = document.querySelectorAll(".related-button");
    for (let i = 0; i < related.length; i++) {
      related[i].addEventListener("click", function(
        e: Event = new Event("click")
      ) {
        self.store.dispatch(
          build(ActionType.CHANGE_ROUTE, {
            route: (e.target as HTMLElement).getAttribute("data-link")
          })
        );
      });
    }

    var newParent = document.querySelectorAll(".add-parent");
    for (var i = 0; i < newParent.length; i++) {
      (newParent[i] || new HTMLElement()).addEventListener("click", function(
        e: Event
      ) {
        self.store.dispatch(
          build(ActionType.ADD_RELATION, {
            parent: (e.target as HTMLElement).getAttribute("data-link"),
            child: doc && doc.node ? doc.node.id : "",
            parentRoleName: Dao.ParentRole.Why,
            childRoleName: Dao.ChildRole.How
          })
        );
      });
    }

    var newChild = document.querySelectorAll(".add-child");
    for (var i = 0; i < newChild.length; i++) {
      newChild[i].addEventListener("click", function(e) {
        self.store.dispatch(
          build(ActionType.ADD_RELATION, {
            parent: doc && doc.node ? doc.node.id : "",
            child: (e.target as HTMLElement).getAttribute("data-link"),
            parentRoleName: "why",
            childRoleName: "how"
          })
        );
      });
    }

    var deleteRelation = document.querySelectorAll(".delete-relation-button");
    for (var i = 0; i < deleteRelation.length; i++) {
      deleteRelation[i].addEventListener("click", function(e) {
        self.store.dispatch(
          build(
            ActionType.DELETE_RELATION,
            (e.target as HTMLElement).getAttribute("data-relation")
          )
        );
      });
    }

    var dismissAlert = document.querySelectorAll(".dismiss-button");
    for (var i = 0; i < dismissAlert.length; i++) {
      dismissAlert[i].addEventListener("click", function(e) {
        self.store.dispatch(
          build(ActionType.DISMISS_ALERT, {
            alertedDoc: doc && doc.node ? doc.node.id : "",
            changedDoc: (e.target as HTMLElement).getAttribute("data-relation")
          })
        );
      });
    }
  }

  private startEditMode(container: HTMLElement, doc: Dao.NodeFamily) {
    const self = this;
    const Template = require("../templates/documentEdit.mustache");
    Util.applyTemplate(container, Template(doc));

    const editorContainer =
      document.getElementById("doc-content") || new HTMLElement();
    const toolbarContainer =
      document.getElementById("toolbar") || new HTMLElement();
    const editor = new Quill(editorContainer);
    editor.addContainer(toolbarContainer);
    if (doc.node && doc.node.content) {
      editor.setText(doc.node.content, "user");
    }

    function saveDocument() {
      var payload: any = {
        title: (document.querySelector("#doc-title") || new HTMLInputElement())
          .nodeValue,
        content: editor.getText()
      };
      if (doc && doc.node && doc.node.id) {
        payload.id = doc.node.id;
      }
      self.store.dispatch(build(ActionType.SAVE_DOC, payload));
      return payload;
    }

    (
      document.querySelector("#save-button") || new HTMLElement()
    ).addEventListener("click", function() {
      saveDocument();
    });
    (
      document.querySelector("#delete-button") || new HTMLElement()
    ).addEventListener("click", function(e) {
      self.store.dispatch(
        build(ActionType.DELETE_DOC, doc && doc.node ? doc.node.id : undefined)
      );
    });
    (
      document.querySelector("#done-button") || new HTMLElement()
    ).addEventListener("click", function() {
      const newDoc = saveDocument();
      self.store.dispatch(build(ActionType.VIEW_DOC, null));
    });
  }

  public render() {
    if (this.store.getState().documentFocus.id !== undefined) {
      this.selectedDoc = Dao.getDocument(
        this.store.getState().documentFocus.id || "",
        false
      ) as Dao.NodeFamily;
    }
    if (this.store.getState().documentFocus.editing) {
      this.startEditMode(
        this.container,
        this.selectedDoc || new Dao.NodeFamily()
      );
    } else {
      this.startViewMode(
        this.container,
        this.selectedDoc || new Dao.NodeFamily()
      );
    }
  }
}
