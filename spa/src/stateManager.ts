import Config from "./config";
import {
  Node,
  Relationship,
  getAllDocuments,
  getAllRelationships,
  ID
} from "./dao";
import * as SeamlessImmutable from "seamless-immutable";

export interface AppState {
  route: string;
  documents: { [key: string]: Node };
  relationships: Relationship[];
  documentFocus: { id?: ID; editing: boolean };
}

const localStorageKey = Config.namespace + ".state";

export const defaultState = SeamlessImmutable<AppState>({
  route: window.location.hash,
  documents: getAllDocuments(),
  relationships: getAllRelationships(),
  documentFocus: { id: undefined, editing: false }
});

export const get: () => SeamlessImmutable.ImmutableObject<AppState> = () => {
  const savedState = JSON.parse(localStorage.getItem(localStorageKey) || "{}");
  return defaultState.merge(savedState);
};

export const set = (state: AppState) =>
  localStorage.setItem(localStorageKey, JSON.stringify(state));
