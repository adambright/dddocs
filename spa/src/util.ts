export const removeClass = (selector: string, className: string) => {
  const links = document.querySelectorAll(selector),
    regex = new RegExp("(?:^|\\s)" + className + "(?!\\S)", "g");
  for (var i = 0; i < links.length; i++) {
    links[i].className = links[i].className.replace(regex, "");
  }
};

export const applyTemplate = (container: Element, renderedTemplate: string) => {
  container.innerHTML = renderedTemplate;
  // window.componentHandler.upgradeDom();
};
