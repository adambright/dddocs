import * as UUID from "node-uuid";
import Config from "./config";

export type ID = string;

export enum NodeType {
  URL,
  Text
}

export class Node {
  id: ID = "";
  name: string = "";
  type: NodeType = NodeType.Text;
  content: string = "";
}

export enum ParentRole {
  Why = "why",
  Value = "value"
}

export enum ChildRole {
  How = "how"
}

export interface Relationship {
  id: ID;
  parent: ID;
  child: ID;
  parentRole: ParentRole;
  childRole: ChildRole;
}

export class NodeFamily {
  public node?: Node = undefined;
  public relationships: { parents: ID[]; children: ID[]; available: Node[] } = {
    parents: [],
    children: [],
    available: []
  };
  public alerts: { [key: string]: {} } = {};

  constructor() {}

  public noChildren(): boolean {
    return this.relationships.children.length === 0;
  }

  public noParents(): boolean {
    return this.relationships.parents.length === 0;
  }

  public noAlerts(): boolean {
    return Object.keys(this.alerts).length > 0;
  }
}

const getItems = (itemName: string) =>
  JSON.parse(localStorage.getItem(Config.namespace + "." + itemName) || "[]");

const saveItems = (itemName: string, items: any) =>
  localStorage.setItem(
    Config.namespace + "." + itemName,
    JSON.stringify(items)
  );

let documents: { [key: string]: Node } = getItems("documents") as {
  [id: string]: any;
};
let relationships: Relationship[] = getItems("relationships");

if (Object.keys(documents).length === 0) {
  // if nothing is found, add some sample documents
  documents = {
    "39802u8apu": {
      name: "Mission Statement",
      content:
        "<div>To make the durable and innovative widgets that delight our customers.</div>",
      id: "39802u8apu",
      type: NodeType.Text
    },
    fda908djafp: {
      name: "Durability",
      content:
        "<div>Our widgets will have a less than 1% fail rate in the first five years of use or our customers get a refund.  Widgets should be robust.</div>",
      id: "fda908djafp",
      type: NodeType.Text
    },
    eowpfdsjl97: {
      name: "Innovation",
      content:
        "<div>We strive for innovative products that exceed customer expectations and competitor's features.</div>",
      id: "eowpfdsjl97",
      type: NodeType.Text
    },
    "9a8ufpupo8": {
      name: "Quality Assurance",
      content:
        "<div>A team will be dedicated to ensuring the durability of our widgets through rigorous testing.</div>",
      id: "9a8ufpupo8",
      type: NodeType.Text
    },
    "4359fadsjls": {
      name: "Speak up for quality program",
      content:
        "<div>All employees are encouraged to alert their managers of any potential defects discovered in our widgets regardless of budgets or schedules.</div>",
      id: "4359fadsjls",
      type: NodeType.Text
    },
    "0f6bd2e0-173c-11e6-89d5-557551a431ee": {
      name: "Skunkworks",
      content:
        "<div>A team of highly talented engineers will be tasked with coming up with innovative product and process improvements that border impossibility.</div>",
      id: "0f6bd2e0-173c-11e6-89d5-557551a431ee",
      type: NodeType.Text
    }
  };

  relationships = [
    {
      id: "39802u8apu",
      parent: "39802u8apu",
      child: "fda908djafp",
      parentRole: ParentRole.Why,
      childRole: ChildRole.How
    },
    {
      id: "fda908djafp",
      parent: "39802u8apu",
      child: "eowpfdsjl97",
      parentRole: ParentRole.Why,
      childRole: ChildRole.How
    },
    {
      id: "eowpfdsjl97",
      parent: "fda908djafp",
      child: "9a8ufpupo8",
      parentRole: ParentRole.Why,
      childRole: ChildRole.How
    },
    {
      id: "pa907alhjh",
      parent: "fda908djafp",
      child: "4359fadsjls",
      parentRole: ParentRole.Why,
      childRole: ChildRole.How
    }
  ];
}

const source: {
  documents: { [key: string]: Node };
  relationships: Relationship[];
  alerts: { [key: string]: { [key: string]: {} } };
} = {
  documents: documents,
  relationships: relationships,
  alerts: getItems("alerts") || {}
};

export const save = () => {
  saveItems("documents", source.documents);
  saveItems("relationships", source.relationships);
  saveItems("alerts", source.alerts);
};

export const getAllDocuments = () => source.documents;

export const getAllRelationships = () => source.relationships;

export const getDocument = (docID: ID, lazy: boolean): Node | NodeFamily => {
  if (!source.documents[docID]) {
    throw new Error("getDocument: node not found");
  }

  if (lazy) {
    return source.documents[docID];
  }

  const selectedDoc: NodeFamily = new NodeFamily();
  selectedDoc.node = source.documents[docID];
  for (var i = 0; i < source.relationships.length; i++) {
    if (source.relationships[i].parent === selectedDoc.node.id) {
      selectedDoc.relationships.children.push(source.relationships[i].child);
    } else if (source.relationships[i].child === selectedDoc.node.id) {
      selectedDoc.relationships.parents.push(source.relationships[i].parent);
    }
  }

  var docKeys = Object.keys(source.documents);
  for (var i = 0; i < docKeys.length; i++) {
    if (docKeys[i] === selectedDoc.node.id) {
      continue;
    }
    var found = false;
    for (var j = 0; j < selectedDoc.relationships.parents.length; j++) {
      if (docKeys[i] === selectedDoc.relationships.parents[j]) {
        found = true;
        break;
      }
    }
    if (found) {
      continue;
    }

    for (var j = 0; j < selectedDoc.relationships.children.length; j++) {
      if (docKeys[i] === selectedDoc.relationships.children[j]) {
        found = true;
        break;
      }
    }
    if (found) {
      continue;
    }

    selectedDoc.relationships.available.push(source.documents[docKeys[i]]);
  }

  selectedDoc.alerts = { ...source.alerts[docID] };

  return selectedDoc;
};

export const saveDocument = (document: Node, relationships: Relationship[]) => {
  if (!document.id) {
    document.id = UUID.v1();
  }
  source.documents[document.id] = document;

  if (relationships !== undefined) {
    for (let i = 0; i < relationships.length; i++) {
      saveRelation(relationships[i]);
    }
  }
  save();
  return document;
};

export const deleteDocument = (id: ID) => {
  delete source.documents[id];
  delete source.alerts[id];
  for (let i = source.relationships.length - 1; i >= 0; i--) {
    if (
      source.relationships[i].parent === id ||
      source.relationships[i].child === id
    ) {
      source.relationships.splice(i, 1);
    }
  }
  save();
};

export const saveRelation = (relationship: Relationship) => {
  if (!relationship.id) {
    relationship.id = UUID.v1();
  }

  source.relationships.push(relationship);
  save();
  return relationship;
};
export const deleteRelation = (relationID: ID) => {
  for (let i = 0; i < source.relationships.length; i++) {
    if (source.relationships[i].id === relationID) {
      source.relationships.splice(i, 1);
      save();
      return;
    }
  }
};

export const addAlert = (
  alertedID: ID,
  sourceID: ID,
  roleName: ParentRole | ChildRole,
  deltas: any[]
) => {
  if (source.alerts[alertedID] == undefined) {
    source.alerts[alertedID] = {};
  }

  source.alerts[alertedID][sourceID] = {
    id: sourceID,
    relatedSlug: ((getDocument(sourceID, true) || { name: "unknown" }) as {
      name: string;
    }).name,
    relatedRoleName: roleName,
    deltas: deltas
  };
  save();
};

export const getAlertCount = (docID: ID) => {
  return source.alerts[docID] ? Object.keys(source.alerts[docID]).length : 0;
};

export const deleteAlert = (alertedID: ID, sourceID: ID) => {
  delete source.alerts[alertedID][sourceID];
  save();
};
