export enum ActionType {
  CHANGE_ROUTE = "CHANGE_ROUTE",
  SET_ACTIVE_DOC = "SET_ACTIVE_DOC",
  SAVE_DOC = "SAVE_DOC",
  EDIT_DOC = "EDIT_DOC",
  VIEW_DOC = "VIEW_DOC",
  DELETE_DOC = "DELETE_DOC",
  ADD_RELATION = "ADD_RELATION",
  DELETE_RELATION = "DELETE_RELATION",
  DISMISS_ALERT = "DISMISS_ALERT"
}

export const build = (action: ActionType, payload?: any) => ({
  type: action,
  payload: payload
});
