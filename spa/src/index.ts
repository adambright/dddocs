import Reducer from "./reducer";
import * as State from "./stateManager";
import * as Views from "./viewManager";
import * as Actions from "./actions";
import { createStore } from "redux";
import { AppState } from "./stateManager";
import { ImmutableObject, Immutable } from "seamless-immutable";

const reducer = createStore(
  Reducer,
  State.get() || State.defaultState,
  undefined
);
Views.setReducer(reducer);
// not sure how this fits into the redux flow but it works for now
window.onhashchange = function(e: HashChangeEvent) {
  var newHash = e.newURL.substr(e.newURL.indexOf("#"));
  if (reducer.getState().route !== newHash) {
    reducer.dispatch(
      Actions.build(Actions.ActionType.CHANGE_ROUTE, { route: newHash })
    );
  }
};
console.log("app started");
