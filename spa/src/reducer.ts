import * as Diff from "diff";
import { replace, ImmutableObject, Immutable } from "seamless-immutable";
import { AppState, defaultState } from "./stateManager";
import { ActionType } from "./actions";
import * as Dao from "./dao";
import { createStore } from "redux";

const handlers: any = {};
handlers[ActionType.CHANGE_ROUTE] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  const newState = state.asMutable({ deep: true });
  newState.route = action.payload.route;
  newState.documentFocus = { id: undefined, editing: true };
  if (
    action.payload.route.indexOf("/documents") !== -1 &&
    action.payload.route.split("/").length > 2
  ) {
    newState.documentFocus = {
      id: action.payload.route.split("/")[2],
      editing: false
    };
  }
  return replace(state, newState);
};

handlers[ActionType.EDIT_DOC] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  return state.setIn(["documentFocus", "editing"], true);
};

handlers[ActionType.VIEW_DOC] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  return state.setIn(["documentFocus", "editing"], false);
};

handlers[ActionType.SAVE_DOC] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  const oldDoc = Dao.getDocument(action.payload.id, false) as Dao.NodeFamily;
  if (
    action.payload.id !== undefined &&
    oldDoc &&
    oldDoc.node &&
    oldDoc.node.content !== action.payload.content
  ) {
    const deltas = Diff.diffWords(oldDoc.node.content, action.payload.content);

    for (let i = 0; i < oldDoc.relationships.parents.length; i++) {
      Dao.addAlert(
        oldDoc.relationships.parents[i],
        action.payload.id,
        Dao.ChildRole.How,
        deltas
      );
    }
    for (let i = 0; i < oldDoc.relationships.children.length; i++) {
      Dao.addAlert(
        oldDoc.relationships.children[i],
        action.payload.id,
        Dao.ParentRole.Why,
        deltas
      );
    }
  }
  var newDoc = Dao.saveDocument(action.payload, []);
  return state.merge({
    route: "#/documents/" + newDoc.id,
    documentFocus: { id: newDoc.id, editing: false },
    documents: Dao.getAllDocuments()
  });
};

handlers[ActionType.DELETE_DOC] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  Dao.deleteDocument(action.payload);
  return state.merge({
    documents: Dao.getAllDocuments(),
    relationships: Dao.getAllRelationships(),
    route: "#/",
    documentFocus: { id: undefined, editing: false }
  });
};

handlers[ActionType.ADD_RELATION] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  Dao.saveRelation(action.payload);
  return state.set("relationships", Dao.getAllRelationships());
};

handlers[ActionType.DELETE_RELATION] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  Dao.deleteRelation(action.payload);
  return state.set("relationships", Dao.getAllRelationships());
};

handlers[ActionType.DISMISS_ALERT] = (
  state: ImmutableObject<AppState>,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  Dao.deleteAlert(action.payload.alertedDoc, action.payload.changedDoc);
  return state;
};

const reducer = (
  state: ImmutableObject<AppState> = defaultState,
  action: { type: string; payload: any }
): Immutable<AppState> => {
  if (handlers[action.type]) {
    return handlers[action.type](state, action);
  }

  return state;
};

export default reducer;
