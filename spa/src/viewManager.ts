import RouteManager from "./views/routeManager";
import * as Master from "./views/master";
import { Store } from "redux";
import { AppState } from "./stateManager";
import { ImmutableObject } from "seamless-immutable";

export const views: { [key: string]: any } = {
  route: { controller: RouteManager, unsubscribe: null },
  master: { controller: Master, unsubscribe: null }
};

export const setReducer = (
  reducer: Store<ImmutableObject<AppState>, { type: string; payload: any }>
) => {
  document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
      return;
    }

    Object.entries(views).forEach(([_, currentView]) => {
      currentView.controller = currentView.controller(reducer);
      currentView.unsubscribe = reducer.subscribe(currentView.controller);
      currentView.controller();
    });
  };
};
