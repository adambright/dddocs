var feedbackButton = document.getElementById("feedback-button"),
  feedbackForm = document.getElementById("feedback-form");

feedbackButton.addEventListener("click", function(e) {
  feedbackButton.style.display = "none";
  feedbackForm.style.display = "block";

  document
    .getElementById("feedback-submit")
    .addEventListener("click", function(e) {
      // make ajax call to submit form data
      feedbackButton.style.display = "block";
      feedbackForm.style.display = "none";
    });
});
